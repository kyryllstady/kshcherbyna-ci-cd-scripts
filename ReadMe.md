### Test 1 ###

ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git st
On branch master
Your branch is ahead of 'origin/master' by 4 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git hist 
* 9e638db 2020-01-31 | add 3commit.txt (HEAD -> master) [kshcherbyna]
* 7982404 2020-01-28 | 2commit [kshcherbyna]
* df8eb2f 2020-01-28 | add 1commit + change ReadMe [kshcherbyna]
* 84f6d42 2020-01-28 | Add new file [Kyryll Shcherbyna]
* 6b3e0a7 2020-01-02 | add ReadMe (origin/master, origin/HEAD) [kyryll]
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git reset --hard 7982404
HEAD is now at 7982404 2commit
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git hist 
* 7982404 2020-01-28 | 2commit (HEAD -> master) [kshcherbyna]
* df8eb2f 2020-01-28 | add 1commit + change ReadMe [kshcherbyna]
* 84f6d42 2020-01-28 | Add new file [Kyryll Shcherbyna]
* 6b3e0a7 2020-01-02 | add ReadMe (origin/master, origin/HEAD) [kyryll]
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git st
On branch master
Your branch is ahead of 'origin/master' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git commit -m "delete commit 9e638db"
On branch master
Your branch is ahead of 'origin/master' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git hist 
* 7982404 2020-01-28 | 2commit (HEAD -> master) [kshcherbyna]
* df8eb2f 2020-01-28 | add 1commit + change ReadMe [kshcherbyna]
* 84f6d42 2020-01-28 | Add new file [Kyryll Shcherbyna]
* 6b3e0a7 2020-01-02 | add ReadMe (origin/master, origin/HEAD) [kyryll]
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git hist --all
* 7982404 2020-01-28 | 2commit (HEAD -> master) [kshcherbyna]
* df8eb2f 2020-01-28 | add 1commit + change ReadMe [kshcherbyna]
* 84f6d42 2020-01-28 | Add new file [Kyryll Shcherbyna]
| * 44de085 2020-01-02 | add develop readme (origin/develop) [kyryll]
|/  
* 6b3e0a7 2020-01-02 | add ReadMe (origin/master, origin/HEAD) [kyryll]
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ touch 3.1commit.txt
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git add -A
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git st
On branch master
Your branch is ahead of 'origin/master' by 3 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   3.1commit.txt

ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git commit -m "fail create file 3.1commit.txt"
[master 34c664c] fail create file 3.1commit.txt
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 3.1commit.txt
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git hist 
* 34c664c 2020-01-31 | fail create file 3.1commit.txt (HEAD -> master) [kshcherbyna]
* 7982404 2020-01-28 | 2commit [kshcherbyna]
* df8eb2f 2020-01-28 | add 1commit + change ReadMe [kshcherbyna]
* 84f6d42 2020-01-28 | Add new file [Kyryll Shcherbyna]
* 6b3e0a7 2020-01-02 | add ReadMe (origin/master, origin/HEAD) [kyryll]
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git revert 34c664c
[master 0e28475] Revert "fail create file 3.1commit.txt"
 1 file changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 3.1commit.txt
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ ll
total 20
drwxrwxr-x 3 ubuntu ubuntu 4096 Jan 31 12:11 ./
drwxrwxr-x 3 ubuntu ubuntu 4096 Jan 28 11:00 ../
drwxrwxr-x 8 ubuntu ubuntu 4096 Jan 31 12:11 .git/
-rw-rw-r-- 1 ubuntu ubuntu    0 Jan 28 12:55 1commit.txt
-rw-rw-r-- 1 ubuntu ubuntu    0 Jan 28 13:06 2commit.txt
-rw-rw-r-- 1 ubuntu ubuntu   17 Jan 28 12:58 ReadMe.md
-rw-rw-r-- 1 ubuntu ubuntu   18 Jan 28 12:49 web-test







### Test2 ###



ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git branch branch_for_delete
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git co branch_for_delete 
Switched to branch 'branch_for_delete'
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git st
On branch branch_for_delete
nothing to commit, working tree clean
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ ll
total 20
drwxrwxr-x 3 ubuntu ubuntu 4096 Jan 31 12:11 ./
drwxrwxr-x 3 ubuntu ubuntu 4096 Jan 28 11:00 ../
drwxrwxr-x 8 ubuntu ubuntu 4096 Jan 31 12:17 .git/
-rw-rw-r-- 1 ubuntu ubuntu    0 Jan 28 12:55 1commit.txt
-rw-rw-r-- 1 ubuntu ubuntu    0 Jan 28 13:06 2commit.txt
-rw-rw-r-- 1 ubuntu ubuntu   17 Jan 28 12:58 ReadMe.md
-rw-rw-r-- 1 ubuntu ubuntu   18 Jan 28 12:49 web-test
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ touch 1br_for_delete.txt
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git st
On branch branch_for_delete
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	1br_for_delete.txt

nothing added to commit but untracked files present (use "git add" to track)
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git add 1br_for_delete.txt 
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git st
On branch branch_for_delete
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   1br_for_delete.txt

ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git commit -m "create 1br_for_delete.txt" 
[branch_for_delete d27b86c] create 1br_for_delete.txt
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 1br_for_delete.txt
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git hist --all
* d27b86c 2020-01-31 | create 1br_for_delete.txt (HEAD -> branch_for_delete) [kshcherbyna]
* 0e28475 2020-01-31 | Revert "fail create file 3.1commit.txt" (origin/master, origin/HEAD, master) [kshcherbyna]
* 34c664c 2020-01-31 | fail create file 3.1commit.txt [kshcherbyna]
* 7982404 2020-01-28 | 2commit [kshcherbyna]
* df8eb2f 2020-01-28 | add 1commit + change ReadMe [kshcherbyna]
* 84f6d42 2020-01-28 | Add new file [Kyryll Shcherbyna]
| * 44de085 2020-01-02 | add develop readme (origin/develop) [kyryll]
|/  
* 6b3e0a7 2020-01-02 | add ReadMe [kyryll]
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git br -a
* branch_for_delete
  master
  remotes/origin/HEAD -> origin/master
  remotes/origin/develop
  remotes/origin/master
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git push origin branch_for_delete 
Counting objects: 2, done.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 258 bytes | 258.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0)
remote: 
remote: To create a merge request for branch_for_delete, visit:
remote:   https://gitlab.com/kyryllstady/kshcherbyna-ci-cd-scripts/-/merge_requests/new?merge_request%5Bsource_branch%5D=branch_for_delete
remote: 
To gitlab.com:kyryllstady/kshcherbyna-ci-cd-scripts.git
 * [new branch]      branch_for_delete -> branch_for_delete
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git branch -a
* branch_for_delete
  master
  remotes/origin/HEAD -> origin/master
  remotes/origin/branch_for_delete
  remotes/origin/develop
  remotes/origin/master
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git co master 
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git br -D branch_for_delete 
Deleted branch branch_for_delete (was d27b86c).
ubuntu@solve1:~/git/kshcherbyna-ci-cd-scripts$ git push origin --delete branch_for_delete
To gitlab.com:kyryllstady/kshcherbyna-ci-cd-scripts.git
 - [deleted]         branch_for_delete
